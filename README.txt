CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Permissions
 * Usage
 * Maintainers

------------
INTRODUCTION
------------

Instafeedjs is a dead-simple way to add Instagram photos to your website. No
jQuery required, just plain javascript.This module ship with the default block
to display the instagram photos.

  * For a full description of the module, visit the project page:
    https://www.drupal.org/node/add/project-issue/instafeedjs

  * To submit bug reports and feature suggestions, or to track changes:
    https://drupal.org/project/instafeed

------------
REQUIREMENTS
------------

Instafeed has one dependency.

Drupal core modules
 * Block

------------
INSTALLATION
------------

  * Instafeedjs can be installed via the standard Drupal installation process
    (http://drupal.org/documentation/install/modules-themes/modules-8).

------------
PERMISSIONS
------------

The ability to create, edit and delete instafeedjs blocks relies on the block
module's "Administer blocks" permission.

-----
USAGE
-----



-----------
MAINTAINERS
-----------

Current Maintainers:

 * Naveen Valecha http://drupal.org/user/2665733
